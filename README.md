Python Schedule Maker
=============

A little python script to pick what to schedule.

Uses a greedy algothrim to select what projects to schedule. 

##Configuration
Example configuration file setup

a 2 40  
b 23 1000  
c 7 170

name time earnings

##Running the Program
	python schedule.py

Note: Be sure to know where the configuration file is located 
